<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view('lamanKu.register');
    }

    public function welkom(Request $request){
        $nama1 = $request['nama1'];
        $nama2 = $request['nama2'];
        return view('lamanKu.welkom', compact('nama1', 'nama2'));
    }
}
