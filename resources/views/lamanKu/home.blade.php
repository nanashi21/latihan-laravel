<!DOCTYPE html>
<html>
    <head>
        <title>Index</title>
    </head>

    <body>
        <header>
            <h1>Media Online</h1>
            <h3>Sosial Media Developer</h3>
            <p>Belajar dan berbagi agar hidup menjadi lebih baik</p>
        </header>

        <div class="content">
            
            <div class="benefit">
                <h4>Benefit Join di Media Online</h4>
                <ul>
                    <li>Mendapatkan motivasi dari sesama para Developer</li>
                    <li>Sharing knowledge</li>
                    <li>Dibuat oleh calon web developer terbaik</li>
                </ul>
            </div>

            <div class="cara_join">
                <h4>Cara Bergabung ke Media Online</h4>
                <ol>
                    <li>Mengunjungi Website ini</li>
                    <li>Mendaftarkan di <a href="/register">Form Sign Up</a></li>
                    <li>Selesai</li>
                </ol>
            </div>
        </div>

    </body>
</html>